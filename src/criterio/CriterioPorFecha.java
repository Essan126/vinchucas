package criterio;

import java.time.LocalDate;

import vinchucas.Muestra;


public abstract class CriterioPorFecha extends CriterioDeBusqueda{
	protected LocalDate fechaInteres;
	
		public CriterioPorFecha(LocalDate _fecha){
			fechaInteres = _fecha; //Sin setter, no quiero que perdure en  el tiempo
		}

			//Template
			@Override
			public boolean cumpleCriterio(Muestra muestra) {
				return this.fechaDeMuestra(muestra).equals(LocalDate.now());
			}	
						
			//Hook
			protected abstract LocalDate fechaDeMuestra(Muestra muestra);
}