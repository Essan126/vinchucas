package criterio;

import java.util.ArrayList;
import java.util.List;

import expresionLogica.ExpresionLogica;
import vinchucas.Muestra;

//Faltan imports

public class Seeker {
	
		public List<Muestra> seekIN(List<Muestra> muestras, ExpresionLogica expresion) {
			List<Muestra> resultado = new ArrayList<Muestra>();
				for(Muestra muestra : muestras){
					if(expresion.getValor(muestra)) {
						resultado.add(muestra);
					}
				}
			return resultado;
		}
}
