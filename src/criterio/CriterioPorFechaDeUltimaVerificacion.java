 package criterio;

import java.time.LocalDate;

import vinchucas.Muestra;

//Falta import 

public class CriterioPorFechaDeUltimaVerificacion extends CriterioPorFecha{
	
		public CriterioPorFechaDeUltimaVerificacion(LocalDate _fecha){
			super(_fecha);
		}

		protected LocalDate fechaDeMuestra(Muestra muestra){
			return muestra.getFechaDeUltimaVerificacion();
		}
}
