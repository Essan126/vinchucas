package criterio;

import java.time.LocalDate;

import vinchucas.Muestra;


public class CriterioPorFechaDeCreacion extends CriterioPorFecha{
	
		public CriterioPorFechaDeCreacion(LocalDate _fecha){
			super(_fecha);
		}

		protected LocalDate fechaDeMuestra(Muestra muestra){
			return muestra.getFechaDeCreacion();
		}
}
