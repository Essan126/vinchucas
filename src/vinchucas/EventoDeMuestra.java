package vinchucas;

import java.time.LocalDate;
import java.util.Date;
import java.util.Observable;

public abstract class EventoDeMuestra {
	protected LocalDate fechaDeEvento;
	protected Muestra muestra;
	protected Usuario usuario;
	protected TipoDeFoto tipoDeFoto;
	
	public EventoDeMuestra(Muestra muestra, Usuario usuario, TipoDeFoto tipoDeFoto) {
		this.muestra = muestra;
		this.usuario = usuario;
		this.tipoDeFoto = tipoDeFoto;
		this.fechaDeEvento = LocalDate.now();
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}
	
	public Muestra getMuestra() {
		return this.muestra;
	}
	
	public LocalDate getFecha() {	//Agregar al UML.
		return this.fechaDeEvento;
	}

	public abstract void ejecutarFuncionalidadCorrespondiente(Organizacion organizacion, Observable zonaDeCobertura);

}
