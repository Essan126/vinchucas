package vinchucas;

import java.util.Observable;
import java.util.Observer;

public class Organizacion implements Observer{
	
	private FuncionalidadExterna funcionalidadDeCreacion;
	private FuncionalidadExterna funcionalidadDeVerificacion;
	private Ubicacion ubicacion;
//	private Integer cantidadDeTrabajadores;  //Comentado porque no se usa para nada.
//	private	String tipoDeOrganizacion;  //Comentado porque no se usa para nada.

	public Organizacion(FuncionalidadExterna funcionalidadDeCreacion, Ubicacion ubicacion,
			FuncionalidadExterna funcionalidadDeVerificacion) {
		this.funcionalidadDeCreacion = funcionalidadDeCreacion;
		this.funcionalidadDeVerificacion = funcionalidadDeVerificacion;
		this.ubicacion = ubicacion;
	}

//	public Integer getCantidadDeTrabajadores() {
//		return this.cantidadDetrabajadores;
//	}

//	public void agregarTrabajador() {
//		this.cantidadDeTrabajadores++;
//	}
	
//	public void removerTrabajador() {
//		this.cantidadDeTrabajadores--;
//	}
	
//	public String getTipoDeOrganizacion() {
//		return this.tipoDeOrganizcion;
//	}

	public void setFuncionalidadDeCreacionDeMuestra(FuncionalidadExterna funcionalidadDeCreacion) {
		this.funcionalidadDeCreacion = funcionalidadDeCreacion;
	}

	public FuncionalidadExterna getFuncionalidadDeCreacionDeMuestra() { // AGREGAR AL UML
		return this.funcionalidadDeCreacion;
	}

	public Ubicacion getUbicacion() {
		return ubicacion;
	}

	public FuncionalidadExterna getFuncionalidadDeVerificacionDeMuestra() { // AGREGAR AL UML
		return this.funcionalidadDeVerificacion;
	}

	public void setFuncionalidadDeVerificacionDeMuestra(FuncionalidadExterna funcionalidadDeVerificacion) {
		this.funcionalidadDeVerificacion = funcionalidadDeVerificacion;
	}

	@Override
	//Agregar clase EventoDeMuestra que sea superclase de EventoDeCreacion y EventoDeVerificacion
	public void update(Observable zonaDeCobertura, Object eventoDeMuestra) { // (tengo q agregar Observer de Java al UML?)
		this.updatePorEvento(zonaDeCobertura, (EventoDeMuestra) eventoDeMuestra);
	}
	
	public void updatePorEvento(Observable zonaDeCobertura, EventoDeMuestra eventoDeMuestra) {
		//revisar esto xq no estoy usando la referencia a "zonaDeCobertura"..
		
		eventoDeMuestra.ejecutarFuncionalidadCorrespondiente(this, zonaDeCobertura);
	}

}
