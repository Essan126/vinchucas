package vinchucas;

public class NivelDeCalificiacion {
	public boolean esCalificable() {
		//dice si la muestra puede ser calificada
		return true;
	}
	
	public boolean verificarCalificacion(Muestra muestra, TipoDeFoto tipo) {
		// hace la verificacion de la calificacion. 
		//Es decir, toma el tipo de foto que dice el usuario calificador y lo compara con el que tiene la muestra.
		return muestra.compararTipoDeFoto(tipo);
	}
	
	public void iniciarCalificacion(Muestra muestra, TipoDeFoto tipo) {
		if(this.verificarCalificacion(muestra, tipo)) {
			this.cambiarCalificacion(muestra);
		}else {
			this.pasarIndeterminada(muestra);
		}
	}

	public void cambiarCalificacion(Muestra muestra) {
		
	}
	
	public void pasarIndeterminada(Muestra muestra) {
		muestra.setNivelCalificacion(new CalificacionIndeterminada());
	}
}
