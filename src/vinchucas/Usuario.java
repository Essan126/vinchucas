package vinchucas;

import java.util.ArrayList;
import java.util.List;

public class Usuario {
	private List<Muestra> muestrasInteractuadas = new ArrayList<Muestra>();
	private String idUsuario;
	private NivelDeUsuario nivelDeUsuario;
	private AppWebDeVinchucas appWeb;
	
	public Usuario (String idUsuario, AppWebDeVinchucas appWeb) {
		this.idUsuario = idUsuario;
		this.nivelDeUsuario = new Basico();
		this.appWeb = appWeb;
		this.appWeb.agregarUsuario(this);
	}
	
	public Usuario (String idUsuario, AppWebDeVinchucas appWeb, NivelDeUsuario nivelDeUsuario) {
		this.idUsuario = idUsuario;
		this.nivelDeUsuario = nivelDeUsuario;
		this.appWeb = appWeb;
		this.appWeb.agregarUsuario(this);
	}
	
	public String getIdUsuario() { // TODO Agregar al UML.
		return this.idUsuario;
	}
	
	public void enviarMuestra(TipoDeFoto tipoDeFoto, Ubicacion ubicacion) {
		this.getNivelDeUsuario().crearMuestra(tipoDeFoto, this, ubicacion, this.appWeb);
//		Muestra nuevaMuestra = new Muestra(tipoDeFoto, this, ubicacion, this.appWeb);
//		this.appWeb.agregarMuestra(nuevaMuestra);
//		this.muestrasInteractuadas.add(nuevaMuestra);
	}
	
	public void verificarMuestra(Muestra muestra, TipoDeFoto tipoDeFoto) {
		if (this.validarVerificacionDeMuestra(muestra)) {
//			muestra.calificar(tipoDeFoto, this);
			this.getNivelDeUsuario().calificarMuestra(muestra, this, tipoDeFoto);
			this.agregarMuestraAInteractuadas(muestra);
		}
	}
	
	public void agregarMuestraAInteractuadas(Muestra muestra) { //TODO Agregar al UML..
		this.muestrasInteractuadas.add(muestra);
	}
	
	public void certificarComoEspecialista() {
		this.setNivelDeUsuario(new Especialista());
	}
	
	private Boolean validarVerificacionDeMuestra(Muestra muestra) {
		return muestra.esCalificablePorUsuario(this);
	}
	
	public NivelDeUsuario getNivelDeUsuario() { //Aca hay q meter magia.
//		return this.nivelDeUsuario;
		return this.nivelDeUsuario.getNivelActualizado(this);
	}
	
	public void setNivelDeUsuario(NivelDeUsuario nivelDeUsuario) {
		this.nivelDeUsuario = nivelDeUsuario;
	}
	
	public AppWebDeVinchucas getAppWeb() {
		return this.appWeb;
	}
	
	@SuppressWarnings("unchecked")
	public List<Muestra> getMuestrasCreadas() { //TODO Agregar al UML.
		return (List<Muestra>) this.muestrasInteractuadas.stream().filter(muestra->muestra.getUsuarioCreador().equals(this.getIdUsuario()));
	}
	
	@SuppressWarnings("unchecked")
	public List<Muestra> getMuestrasVerificadas() { //TODO Agregar al UML.
		return (List<Muestra>) this.muestrasInteractuadas.stream().filter(muestra->!muestra.getUsuarioCreador().equals(this.getIdUsuario()));
	}
}
