package vinchucas;

import java.util.ArrayList;
import java.util.List;

public class Muestra {
	//los atributos de la muestra son todos privados, y para conocer o escribir los atributos, las otras clases deben usar los setters y getters.
	private TipoDeFoto tipoDeVinchuca;  								//la muestra dice si contiene un bicho o no, sobre la cual se hacen las verificaciones.
	private Foto foto;             										//la muestra tiene una foto asociada
	private Ubicacion ubicacion;   										//la muestra sabe la long y lat donde fue tomada
	private Usuario usuarioCreador;  									//guardo una referencia al usuario que crea la muestra
	private NivelDeCalificiacion nivelDeCalificacion;  					//guardo el nivel de calificacion de la muestra
	private List<Usuario> calificadores = new ArrayList<Usuario>(); 	//guardo una lista con los dos usuarios que califican
	
	public Muestra(TipoDeFoto tipoDeVinchuca, Foto foto, Ubicacion ubicacion,  Usuario usuarioCreador, AppWebDeVinchucas appWeb) {
		//constructor de Muestra.
		this.tipoDeVinchuca = tipoDeVinchuca;
		this.foto = foto;
		this.ubicacion = ubicacion;
		this.usuarioCreador = usuarioCreador;
		this.nivelDeCalificacion = new CalificacionBaja();
	}
	
	public TipoDeFoto getTipoDeVinchuca() {
		return this.tipoDeVinchuca;
	}
	
	public boolean compararTipoDeFoto(TipoDeFoto tipo) {
		return tipo==this.getTipoDeVinchuca();
	}
	
	public Ubicacion getUbicacionMuestra() {
		return this.ubicacion;
	}
	
	public Usuario getUsuarioCreador() {
		return this.usuarioCreador;
	}
	
	public NivelDeCalificiacion getNivelDeCalificacion() {
		//getter del nivel de calificacion
		return this.nivelDeCalificacion;
	}

	public void setNivelCalificacion(NivelDeCalificiacion nuevoNivel) {
		this.nivelDeCalificacion= nuevoNivel;
	}
	
	public void calificarMuestra(Muestra muestra, Usuario usuario, TipoDeFoto tipoDeFoto) {
		//cambio el nivel de calificacion de la muestra
		/* 1- Valido si la foto se puede calificar
		 * 2- La califico.
		 */
		if(validarCalificar(usuario)) {
			this.nivelDeCalificacion.iniciarCalificacion(this, tipoDeFoto);
		}
	}
	
	private boolean validarCalificar(Usuario usuario) {
		// si la foto es calificable por el usuario y est� en un nivel de calificaci�n que permite ser calificada, retorno true.
		return this.esCalificablePorUsuario(usuario)&&this.nivelDeCalificacion.esCalificable();
	}
	
	public List<Usuario> getListaCalificadores() {
		return this.calificadores;
	}
	
	public boolean esCalificablePorUsuario(Usuario usuario) {
		// devuelve true si el usuario creador es distinto al usuario que quiere calificar, y este no la calific� todav�a.
		return !(esUsuarioCreador(usuario)||usuarioCalifico(this.getListaCalificadores(), usuario)) ;
	}

	private boolean esUsuarioCreador(Usuario usuario) {
		// devuelve true si el usuario creador es igual al usuario del parametro.
		return this.getUsuarioCreador() == usuario;
	}
	
	public boolean usuarioCalifico(final List<Usuario> calificadores, final Usuario usuario){
		// devuelve true si el usuario del parametro se encuentra en la lista de claificadores.
		//recorro el array de calificaciones y veo si el usuario del par�metro es igual a alguno de los otros dos.
		return calificadores.stream().filter(o -> o.equals(usuario)).findFirst().isPresent();
	}
	
}
