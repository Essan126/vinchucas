package vinchucas;

import java.time.LocalDate;

public abstract class NivelDeUsuario { //TODO Modificar en UML para q este como aca. (repasar todas las clases por las dudas)
	
	protected String identificacion;
	
	protected Integer cantMinDeMuestrasCreadasParaExperto = 11;
	protected Integer cantMinDeMuestrasVerificadasParaExperto = 21;
	
	public abstract void calificarMuestra(Muestra muestra, Usuario usuario, TipoDeFoto tipoDeFoto);

	public NivelDeUsuario getNivelActualizado(Usuario usuario) {
		if (this.cantidadDeMuestrasCreadasEnElUltimoMes(usuario) >= cantMinDeMuestrasCreadasParaExperto && this.cantidadDeMuestrasVerificadasEnElUltimoMes(usuario) >= cantMinDeMuestrasVerificadasParaExperto) {
			return this.estadoExperto();
		} else {
			return this.estadoBasico();
		}
	}

	public void crearMuestra(TipoDeFoto tipoDeFoto, Foto foto, Ubicacion ubicacion, Usuario usuario, AppWebDeVinchucas appWeb) { //TODO Agregar al UML..como todo lo demas..
		Muestra nuevaMuestra = new Muestra(tipoDeFoto, foto, ubicacion, usuario, appWeb);
		appWeb.agregarMuestra(nuevaMuestra);
		usuario.agregarMuestraAInteractuadas(nuevaMuestra);
	}
	
	protected Integer cantidadDeMuestrasVerificadasEnElUltimoMes(Usuario usuario) {
		return (int) usuario.getMuestrasCreadas().stream().filter(muestra->muestra.getEventoDeCreacion().getFecha().getMonth().equals(LocalDate.now().getMonth())).count();
	}
	
	protected Integer cantidadDeMuestrasCreadasEnElUltimoMes(Usuario usuario) {
		return (int) usuario.getMuestrasVerificadas().stream().filter(muestra->muestra.getVerificacionDeUsuario(usuario).getFecha().getMonth().equals(LocalDate.now().getMonth())).count();
	}
	
	protected NivelDeUsuario estadoExperto() {
		return new Experto();
	}
	
	protected NivelDeUsuario estadoBasico() {
		return new Basico();
	}
	
	public String getId() {
		return this.identificacion;
	}
}
