package vinchucas;

import java.util.Observable;

public class EventoDeCreacion extends EventoDeMuestra{

	public EventoDeCreacion(Muestra muestra, Usuario usuario, TipoDeFoto tipoDeFoto) {
		super(muestra, usuario, tipoDeFoto);
		// TODO Auto-generated constructor stub
	} //Agregar EventoDeMuestra al UML
	
	public Usuario getCreador() {
		return this.getUsuario();
	}

	@Override
	public void ejecutarFuncionalidadCorrespondiente(Organizacion organizacion, Observable zonaDeCobertura) {
		organizacion.getFuncionalidadDeCreacionDeMuestra().nuevoEvento(organizacion, (ZonaDeCobertura) zonaDeCobertura, muestra);
	}

}
