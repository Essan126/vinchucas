package vinchucas;

public class CalificacionBaja extends NivelDeCalificiacion {
	
	public void cambiarCalificacion(Muestra muestra) {
		muestra.setNivelCalificacion(new CalificacionMedia());
	}
	
}
