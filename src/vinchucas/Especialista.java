package vinchucas;

public class Especialista extends Experto {
	
	public Especialista() {
		this.identificacion = "Especialista";
	}

	@Override
	public NivelDeUsuario getNivelActualizado(Usuario usuario) {
		return this;
	}

}
