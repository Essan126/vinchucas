package vinchucas;

public class Experto extends NivelDeUsuario {
	
	public Experto() {
		this.identificacion = "Experto";
	}

	@Override
	public void calificarMuestra(Muestra muestra, Usuario usuario, TipoDeFoto tipoDeFoto) {
		muestra.setTipoDeFoto(tipoDeFoto);
		muestra.setNivelDeCalificacion(new Alta());
	}
	
	@Override
	protected NivelDeUsuario estadoExperto() {
		return this;
	}
}
