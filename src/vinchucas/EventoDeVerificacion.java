package vinchucas;

//import java.util.Date;
import java.util.Observable;

public class EventoDeVerificacion extends EventoDeMuestra{
	
//	private Date fechaDeVerificacion;
//	private Muestra muestra;
//	private Usuario verificador;
	
	public EventoDeVerificacion(Muestra muestra, Usuario verificador, TipoDeFoto tipoDeFoto) {
		super(muestra, verificador, tipoDeFoto);
//		this.muestra = muestra;
//		this.verificador = verificador;
//		this.fechaDeVerificacion = new Date();
	}

	public Usuario getVerificador() {
		return this.getUsuario();
	}
	
	@Override
	public void ejecutarFuncionalidadCorrespondiente(Organizacion organizacion, Observable zonaDeCobertura) {
		organizacion.getFuncionalidadDeVerificacionDeMuestra().nuevoEvento(organizacion, (ZonaDeCobertura) zonaDeCobertura, muestra);
	}

}
