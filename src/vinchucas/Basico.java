package vinchucas;

import java.time.LocalDate;

public class Basico extends NivelDeUsuario {
	
	public Basico() {
		this.identificacion = "Basico";
	}

	@Override
	public void calificarMuestra(Muestra muestra, Usuario usuario, TipoDeFoto tipoDeFoto) {
		muestra.calificarMuestra(muestra, usuario, tipoDeFoto);
	}
	
	@Override
	protected NivelDeUsuario estadoBasico() {
		return this;
	}
}
