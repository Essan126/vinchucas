package vinchucas;

import java.util.ArrayList;
import java.util.List;

import criterio.CriterioDeBusqueda;

public class AppWebDeVinchucas {
	private List<Muestra> muestras = new ArrayList<Muestra>();
	private List<Usuario> usuarios = new ArrayList<Usuario>();
	private List<ZonaDeCobertura> zonasDeCobertura = new ArrayList<ZonaDeCobertura>();

	public void agregarUsuario(Usuario usuario) {
		if (!this.usuarios.contains(usuario)) {
			this.usuarios.add(usuario);
		}
	}
	
	public void agregarMuestra(Muestra muestra) {
		if (!this.muestras.contains(muestra)) {
			this.muestras.add(muestra);
		}
	}
	
	public void agregarZonaDeCobertura(ZonaDeCobertura zonaDeCobertura) {
		if (!this.zonasDeCobertura.contains(zonaDeCobertura)) {
			this.zonasDeCobertura.add(zonaDeCobertura);
		}
	}
	
	public List<Muestra> busquedaDeMuestras(CriterioDeBusqueda criterioDeBusqueda) {
		return new ArrayList<Muestra>(); //TODO tengo q hacer este metodo, se dejo asi para q no marque error.
	}
}
