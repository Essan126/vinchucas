package expresionLogica;

import vinchucas.Muestra;


public interface ExpresionLogica {
	
	public Boolean getValor(Muestra muestra);

}
