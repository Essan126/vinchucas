package vinchucas;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import java.util.Observable;

public class OrganizacionTest {
	
	Organizacion organizacionSUT;
	Ubicacion ubicacionMockeada;
	FuncionalidadExterna funcionalidadExterna;
	
	@Before
	public void setUp() throws Exception {
		ubicacionMockeada = mock(Ubicacion.class);
		funcionalidadExterna = mock(FuncionalidadExterna.class);
		organizacionSUT = new Organizacion(funcionalidadExterna, ubicacionMockeada, funcionalidadExterna);
	}

	@Test
	public void testCreacionOrganizacion() {
		assertEquals(ubicacionMockeada, organizacionSUT.getUbicacion());
		assertEquals(funcionalidadExterna, organizacionSUT.getFuncionalidadDeCreacionDeMuestra());
		assertEquals(funcionalidadExterna, organizacionSUT.getFuncionalidadDeVerificacionDeMuestra());
	}
	
	@Test
	public void testCantidadDeTrabajadoresInicial() {
		assertEquals((Integer)0, organizacionSUT.getCantidadDeTrabajadores());
	}
	
	@Test
	public void testCantidadDeTrabajadoresEsCorrectaAlAumentarla() {
		organizacionSUT.agregarTrabajador(mock(Trabajador.class));
		assertEquals((Integer)1, organizacionSUT.getCantidadDeTrabajadores());
		organizacionSUT.agregarTrabajador(mock(Trabajador.class));
		organizacionSUT.agregarTrabajador(mock(Trabajador.class));
		assertEquals((Integer)3, organizacionSUT.getCantidadDeTrabajadores());	
	}
	
	@Test
	public void testCambioFuncionalidades() {
		  //Funcionalidad de creacion.
		FuncionalidadExterna funcionalidadExternaNueva = mock(FuncionalidadExterna.class);
		organizacionSUT.setFuncionalidadDeCreacionDeMuestra(funcionalidadExternaNueva);
		assertEquals(funcionalidadExternaNueva, organizacionSUT.getFuncionalidadDeCreacionDeMuestra());
		
		FuncionalidadExterna funcionalidadExternaNueva2 = mock(FuncionalidadExterna.class);
		organizacionSUT.setFuncionalidadDeCreacionDeMuestra(funcionalidadExternaNueva2);
		assertEquals(funcionalidadExternaNueva2, organizacionSUT.getFuncionalidadDeCreacionDeMuestra());
		
		  //Funcionalidad de verificacion.
		organizacionSUT.setFuncionalidadDeVerificacionDeMuestra(funcionalidadExternaNueva);
		assertEquals(funcionalidadExternaNueva, organizacionSUT.getFuncionalidadDeVerificacionDeMuestra());
		
		organizacionSUT.setFuncionalidadDeVerificacionDeMuestra(funcionalidadExternaNueva2);
		assertEquals(funcionalidadExternaNueva2, organizacionSUT.getFuncionalidadDeVerificacionDeMuestra());
	}
	
	@Test
	public void testDeLlamadoDeFuncionCorrectaAlRecibirElMensajeDeUpdate() {
		FuncionalidadExterna funcionalidadDeVerificacion = mock(FuncionalidadExterna.class);
		FuncionalidadExterna funcionalidadDeVerificacionSPY = spy(funcionalidadDeVerificacion);
		
		FuncionalidadExterna funcionalidadDeCreacion = mock(FuncionalidadExterna.class);
		FuncionalidadExterna funcionalidadDeCreacionSPY = spy(funcionalidadDeVerificacion);
		
		organizacionSUT.setFuncionalidadDeCreacionDeMuestra(funcionalidadDeCreacionSPY);
		organizacionSUT.setFuncionalidadDeVerificacionDeMuestra(funcionalidadDeVerificacionSPY);
		
		Muestra muestra = mock(Muestra.class); //Tengo q meter la muestra en la lista de muestras de la zonaDeCoberturaX? O algo mas para validar q sea la muestra que dispara el evento?..
		ZonaDeCobertura zonaDeCoberturaX = mock(ZonaDeCobertura.class);
		Object eventoDeVerificacion = mock(EventoDeVerificacion.class);
		Object eventoDeCreacion = mock(EventoDeCreacion.class);
		
		organizacionSUT.update(zonaDeCoberturaX, eventoDeVerificacion);
		verify(funcionalidadDeVerificacionSPY).nuevoEvento(organizacionSUT, zonaDeCoberturaX, muestra);
		verifyZeroInteractions(funcionalidadDeCreacionSPY);
		
		organizacionSUT.update(zonaDeCoberturaX,  eventoDeCreacion);
		verify(funcionalidadDeCreacionSPY).nuevoEvento(organizacionSUT, zonaDeCoberturaX, muestra);
		verifyZeroInteractions(funcionalidadDeVerificacionSPY);
	}

}
